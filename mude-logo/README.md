# MUDE Logos

The logos was created in August 2022. Justin has the source files and has made updates to them. There are 3 variations:
- size
- color
- with/without text

There is a special logo for the Answers platform.

This README and the logo files are stored in GitLab (https://gitlab.tudelft.nl/mude/public/) and Teams (Files/General/Media...).
